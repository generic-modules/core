<?php // src/Form/Users/Management/RegisterType.php

namespace Magnetar\CoreBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    /**
     * {@inheritDoc}
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstname',TextType::class,['label' => 'form.user.firstname']);
        $builder->add('lastname',TextType::class,['label' => 'form.user.lastname']);
        $builder->add('email',TextType::class,['label' => 'form.user.email']);
        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'first_options'  => ['label' => 'form.user.password'],
            'second_options' => ['label' => 'form.user.repeat_password'],
        ]);
        $builder->add('save',SubmitType::class,['label' => 'form.user.submit']);
    }
}
