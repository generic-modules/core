<?php
// src/AppBundle/Entity/User.php

namespace Magnetar\CoreBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Magnetar\CoreBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @var int
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string",nullable=true)
     */
    protected $firstName;
    
    /**
     * @var string
     * 
     * @ORM\Column(type="string",nullable=true)
     */
    protected $lastName;
    
    /**
     * Constructor.
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * {@inheritDoc}
     * @see \FOS\UserBundle\Model\User::setUsername()
     */
    public function setEmail($email)
    {
        parent::setEmail($email);
        $this->username = $this->email;
    }
    
    /**
     * Set first name.
     * 
     * @param string $firstName
     * 
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    
    /**
     * Get first name.
     * 
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Set last name.
     * 
     * @param string $lastName
     * 
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    
    /**
     * Get last name
     * 
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
}





