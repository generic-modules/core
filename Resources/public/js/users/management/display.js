$(document).ready(function(){
	$('#remove-button').on('click',function(e){
		e.preventDefault();
		var button = $(this);
		$.bsAlert.confirmTitle = 'Usunięcie konta';
		$.bsAlert.closeDisplay = 'Nie';
		$.bsAlert.sureDisplay = 'Tak';
		$.bsAlert.confirm("Czy na pewno chcesz usunąć konto?",function(){
			$(location).attr('href', button.attr('href'))
		});
	});
});
