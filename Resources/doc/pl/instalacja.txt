Instrukcja instalacji.

1. Umieścić katalog docroot w katalogu docroot serwera www albo ustawić virtual host dla tego katalogu.
2. Ustawić w skrypcie index.php zmienne: $application, $modules, $system.
Są to odpowiednio:
- ścieżka do katalogu aplikacji (skrypty php dla konkretnego wdrożenia)
- ścieżka do katalogu modułów (skrypty php modułów)
- ścieżka do katalogu frameworka KohanaPHP 3.1.4.
