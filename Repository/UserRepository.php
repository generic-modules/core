<?php 

namespace Magnetar\CoreBundle\Repository;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Magnetar\CoreBundle\Entity\User;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
        $this->em = $this->getEntityManager();
    }
    
    /**
     * Update user data
     * 
     * @param User $user user entity
     * 
     * @return void
     */
    public function update(User $user)
    {
        $this->em->persist($user);
        $this->em->flush();
    }
    
    /**
     * Remove user
     * 
     * @param User $user user entity
     * 
     * @return void
     */
    public function remove(User $user)
    {
        $user->setEnabled(false);
        $this->em->persist($user);
        $this->em->flush();
    }
}
