<?php // src/Service/Email/Register.php

namespace Magnetar\CoreBundle\Service\Email;


use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Register
{
    /**
     * Twig_Environment $twig
     */
    protected $twig;
    
    /**
     * Swift_Mailer $mailer
     */
    protected $mailer;
    
    
    protected $router;
    
    
    public function __construct(\Twig_Environment $twig,\Swift_Mailer $mailer, UrlGeneratorInterface $router, ParameterBagInterface $parameters)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->parameters = $parameters;
    }
    
    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $senderName = $this->parameters->get('mailer_user');
        $template = '@FOSUser/Registration/email.txt.twig';
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);
        $context = array(
            'user' => $user,
            'confirmationUrl' => $url,
        );
        $this->sendMessage($template, $context, $senderName, (string) $user->getEmail());
    }
    
    protected function sendMessage($templateName, $context, $fromEmail, $toEmail)
    {
        $template = $this->twig->load($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);
        $htmlBody = '';
        if ($template->hasBlock('body_html', $context)) {
            $htmlBody = $template->renderBlock('body_html', $context);
        }
        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
        ;
        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }
        $this->mailer->send($message);
    }
}
