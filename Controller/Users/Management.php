<?php // src/Controller/Users/Management.php

namespace Magnetar\CoreBundle\Controller\Users;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Magnetar\CoreBundle\Controller\Controller;
use Magnetar\CoreBundle\Form\UserType;
use Magnetar\CoreBundle\Entity\User;

class Management extends Controller
{
    /**
     * Display user data.
     * 
     * @Route("users/management/display")
     */
    public function display()
    {
        $user = $this->getUser();
        return $this->render('@MagnetarCoreBundle/users/management/display.html.twig',[
            'user' => $user,
        ]);
    }
    
    /**
     * Edit user.
     * 
     * @Route("users/management/edit")
     */
    public function edit(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $this->getDoctrine()->getRepository(User::class)->update($user);
            $request->getSession()->getFlashBag()->add(
                'notice',
                $this->get('translator')->trans('users.management.edit.confirmation')
            );
        }
        return $this->render('@MagnetarCoreBundle/users/management/edit.html.twig',[
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Remove user.
     * 
     * @Route("users/management/remove")
     */
    public function remove()
    {
        $this->getDoctrine()->getRepository(User::class)->remove($this->getUser());
        return $this->render('@MagnetarCoreBundle/users/management/remove.html.twig');
    }
}



