<?php // src/Controller/Users/Other/Management.php

namespace Magnetar\CoreBundle\Controller\Users\Other;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Magnetar\CoreBundle\Controller\Controller;
use Magnetar\CoreBundle\Form\UserType;
use Magnetar\CoreBundle\Entity\User;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Magnetar\CoreBundle\Service\Email\Register as RegisterEmail;
use FOS\UserBundle\Util\TokenGeneratorInterface;
// use FOS\UserBundle\Util\TokenGenerator;

class Management extends Controller
{
    /**
     * Create account for another user.
     * 
     * @Route("users/other/management/create")
     */
    public function create(Request $request, UserManagerInterface $userManager, RegisterEmail $email, TokenGeneratorInterface $tokenGenerator)
    {
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user->setPassword($tokenGenerator->generateToken());
                $user->setUsername($user->getEmail());
                $user->setConfirmationToken($tokenGenerator->generateToken());
                $userManager->updateUser($user);
                $email->sendConfirmationEmailMessage($user);
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    $this->get('translator')->trans('users.other.management.create.confirmation')
                );
                return $this->redirect($this->generateUrl('magnetar_core_users_other_management_display',['id' => $user->getId()]));
            }
        }
        return $this->render('@MagnetarCoreBundle/users/other/management/create.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * Display user data.
     * 
     * @Route("users/other/management/display/{id}")
     */
    public function display($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@MagnetarCoreBundle/users/other/management/display.html.twig',[
            'user' => $user,
        ]);
    }
}
