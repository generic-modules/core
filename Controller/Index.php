<?php // src/Controller/Index.php

namespace Magnetar\CoreBundle\Controller;


use Magnetar\CoreBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class Index extends Controller
{

    /**
     * Main page.
     * 
     * @return Response
     */
    public function index()
    {
        return $this->render('@MagnetarCoreBundle/index.html.twig');
    }
}
